#pragma once
#include <iostream>
#include "controller/PiecesController.h"

class UI
{
private:
	PiecesController* m_controller;

	static void PrintMenu();

	static void PrintPieces(std::vector<ChessPiece*> pieces);

	static int ReadInteger(std::string message);
	static std::string ReadString(std::string message);

	static ChessPiece::Color ReadColor();
	static Knight::HorseBreeds ReadBreed();

public:
	UI(PiecesController* controller);

	void Run();

	~UI();
};

