#include "UI.h"

void UI::PrintMenu()
{
	std::string menu = 
		"m. Show menu\n"
		"1. Show all pieces\n"
		"2. Show specific pieces\n"
		"3. Show pieces of color\n"
		"4. Add pawn\n"
		"5. Add knight\n"
		"6. Add king\n"
		"7. Delete piece by id\n"
		"8. Undo\n"
		"9. Redo\n"
		"0. Exit"
	;
	std::cout << menu << std::endl;
}

void UI::PrintPieces(std::vector<ChessPiece*> pieces)
{
	for (ChessPiece* piece : pieces)
	{
		std::cout << piece->ToString() << std::endl;
	}
}

int UI::ReadInteger(std::string message)
{
	std::cout << message;
	int a;
	std::cin >> a;
	while (std::cin.fail())
	{
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		std::cout << "The value has to be an integer" << std::endl;
		std::cin >> a;
	}
	return a;
}

std::string UI::ReadString(std::string message)
{
	std::cout << message;
	std::string a;
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	std::getline(std::cin, a);
	return a;
}

ChessPiece::Color UI::ReadColor()
{
	std::string possibleColors =
		std::to_string(ChessPiece::White) + ". " + ChessPiece::ColorToString(ChessPiece::White) + "\n" +
		std::to_string(ChessPiece::Black) + ". " + ChessPiece::ColorToString(ChessPiece::Black);

	std::cout << possibleColors << std::endl;
	std::cout << "Color number: ";

	int c;
	std::cin >> c;
	while (1)
	{
		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << "The value has to be an integer" << std::endl;
			std::cin >> c;
		}
		else if (c != ChessPiece::Black && c != ChessPiece::White)
		{
			std::cout << "The value is not valid" << std::endl;
			std::cin >> c;
		}
		else
		{
			break;
		}
	}
	return static_cast<ChessPiece::Color>(c);
}

Knight::HorseBreeds UI::ReadBreed()
{
	std::string possibleBreeds =
		std::to_string(Knight::Arabian) + ". " + Knight::BreedToString(Knight::Arabian) + "\n" +
		std::to_string(Knight::Appaloosa) + ". " + Knight::BreedToString(Knight::Appaloosa) + "\n" +
		std::to_string(Knight::Morgan) + ". " + Knight::BreedToString(Knight::Morgan);

	std::cout << possibleBreeds << std::endl;
	std::cout << "Breed number: ";

	int c;
	std::cin >> c;
	while (1)
	{
		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << "The value has to be an integer" << std::endl;
			std::cin >> c;
		}
		else if (c != Knight::Arabian && c != Knight::Appaloosa && c != Knight::Morgan)
		{
			std::cout << "The value is not valid" << std::endl;
			std::cin >> c;
		}
		else
		{
			break;
		}
	}
	return static_cast<Knight::HorseBreeds>(c);
}

UI::UI(PiecesController* controller) : m_controller(controller)
{
}

void UI::Run()
{
	UI::PrintMenu();
	std::string command;

	while (1)
	{
		std::cout << ">>> ";
		std::cin >> command;
		try
		{
			if (command == "m")
			{
				PrintMenu();
			}
			else if (command == "1")
			{
				PrintPieces(m_controller->GetAllPieces());
			}
			else if (command == "2")
			{
				PrintPieces(m_controller->GetPiecesByType(UI::ReadString("Piece type (e.g Pawn, Knight, King): ")));
			}
			else if (command == "3")
			{
				PrintPieces(m_controller->GetPiecesByColor(UI::ReadColor()));
			}
			else if (command == "4")
			{
				Point piecePosition(UI::ReadInteger("X: "), UI::ReadInteger("Y: "));
				m_controller->AddPiece(new Pawn(piecePosition, UI::ReadColor(), UI::ReadInteger("Height: ")));
			}
			else if (command == "5")
			{
				Point piecePosition(UI::ReadInteger("X: "), UI::ReadInteger("Y: "));
				m_controller->AddPiece(new Knight(piecePosition, UI::ReadColor(), UI::ReadBreed()));
			}
			else if (command == "6")
			{
				Point piecePosition(UI::ReadInteger("X: "), UI::ReadInteger("Y: "));
				m_controller->AddPiece(new King(piecePosition, UI::ReadColor(), UI::ReadString("Kingdom name: ")));
			}
			else if (command == "7")
			{
				m_controller->RemovePieceById(UI::ReadInteger("Id of piece to be deleted: "));
			}
			else if (command == "8")
			{
				m_controller->Undo();
			}
			else if (command == "9")
			{
				m_controller->Redo();
			}
			else if (command == "0")
			{
				break;
			}
			else
			{
				std::cout << "Invalid command" << std::endl;
			}
		}
		catch (InvalidIdException e)
		{
			std::cout << e.what() << std::endl;
		}
		catch (OccupiedPositionException e)
		{
			std::cout << e.what() << std::endl;
		}
		catch (OutOfBoundsPositionException e)
		{
			std::cout << e.what() << std::endl;
		}
		catch (CorruptedDatabaseException e)
		{
			std::cout << "The database couldn't be parsed. Either cleanup the database or continue with the application without any saved data.";
		}
	}
}

UI::~UI()
{
	delete m_controller;
}
