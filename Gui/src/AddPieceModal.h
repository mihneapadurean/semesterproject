#pragma once

#include <qmainwindow.h>
#include <qformlayout.h>
#include <qlineedit.h>
#include <qcombobox.h>
#include <qpushbutton.h>
#include <qlabel.h>

#include "Dashboard.h"

class AddPieceModal : public QMainWindow
{
	Q_OBJECT
private:
	Dashboard* m_parentWindow;

	QFormLayout* m_formLayout;

	QComboBox* m_typeDropdown;
	QComboBox* m_colorDropdown;
	QLineEdit* m_xPosition;
	QLineEdit* m_yPosition;

	QLabel* m_kingdomNameLabel;
	QLineEdit* m_kingdomName;

	QLabel* m_knightBreedLabel;
	QComboBox* m_knightBreedDropdown;

	QLabel* m_pawnHeightLabel;
	QLineEdit* m_pawnHeight;

	void setupModal();

private slots:
	void updateFormForType(int typeIndex);
	void savePiece();
public:
	AddPieceModal(Dashboard* parentWindow);
	~AddPieceModal();
};

