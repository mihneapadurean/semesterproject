#pragma once

#include <vector>
#include <array>

#include <qmainwindow.h>
#include <qlistwidget.h>
#include <qpushbutton.h>
#include <QKeyEvent>
#include <qcombobox.h>

#include "controller/PiecesController.h"

/**
*	Entry point for UI, it's the class which represents the main window of the application
*/
class Dashboard : public QMainWindow
{
	Q_OBJECT
private:
	PiecesController* m_controller;
	QListWidget* m_piecesList;
	QPushButton* removePieceButton;

	QComboBox* m_typesDropdown;
	QComboBox* m_colorsDropdown;

	void setupDahsboard();
	void showAddPieceModal();

	void renderPiecesList(std::vector<ChessPiece*> pieces);

	void keyReleaseEvent(QKeyEvent* event) override;

	static std::array<std::string, 3> indexToTypeName;

private slots:
	void deletePiece();
	void filterPieces();

public:
	/**
	*	@params controller - PiecesController
	*/
	Dashboard(PiecesController* controller, QWidget* parent = nullptr);
	~Dashboard();

	/**
	*	Method to be used by children to save pieces to the repository (e.g. the form for adding will call this method
	*	access the add from the controller)
	*	@params piece - ChessPiece*
	*/
	void SavePiece(ChessPiece* piece);
};
