#include <QVBoxLayout>
#include <QHBoxLayout>
#include <qlabel.h>

#include "Dashboard.h"
#include "AddPieceModal.h"
#include "helpers/PieceListItem.h"

std::array<std::string, 3> Dashboard::indexToTypeName = {Pawn::Type(), Knight::Type(), King::Type()};

void Dashboard::setupDahsboard()
{
	QWidget* centralWidget = new QWidget(this);
	QVBoxLayout* mainLayout = new QVBoxLayout(centralWidget);

	QLabel* title = new QLabel("Chess pieces on the table", centralWidget);
	title->setAlignment(Qt::AlignCenter);
	mainLayout->addWidget(title);

	QHBoxLayout* piecesTypesLayout = new QHBoxLayout(centralWidget);
	QLabel* piecesTypesShown = new QLabel("Types shown:", centralWidget);
	piecesTypesShown->setFixedHeight(100);
	m_typesDropdown = new QComboBox(centralWidget);
	m_typesDropdown->setFixedWidth(100);
	m_typesDropdown->insertItem(0, QString::fromStdString(Dashboard::indexToTypeName[0]));
	m_typesDropdown->insertItem(1, QString::fromStdString(Dashboard::indexToTypeName[1]));
	m_typesDropdown->insertItem(2, QString::fromStdString(Dashboard::indexToTypeName[2]));
	m_typesDropdown->insertItem(3, "All");
	m_typesDropdown->setCurrentIndex(3);

	piecesTypesLayout->addWidget(piecesTypesShown);
	piecesTypesLayout->addWidget(m_typesDropdown);
	mainLayout->addLayout(piecesTypesLayout);

	connect(m_typesDropdown, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &Dashboard::filterPieces);

	QHBoxLayout* piecesColorsLayout = new QHBoxLayout(centralWidget);
	QLabel* piecesColorsShown = new QLabel("Colors shown: ", centralWidget);
	piecesColorsShown->setFixedHeight(100);
	m_colorsDropdown = new QComboBox(centralWidget);
	m_colorsDropdown->insertItem(ChessPiece::White, "White");
	m_colorsDropdown->insertItem(ChessPiece::Black, "Black");
	m_colorsDropdown->insertItem(2, "All");
	m_colorsDropdown->setCurrentIndex(2);

	piecesColorsLayout->addWidget(piecesColorsShown);
	piecesColorsLayout->addWidget(m_colorsDropdown);
	mainLayout->addLayout(piecesColorsLayout);

	connect(m_colorsDropdown, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &Dashboard::filterPieces);

	QHBoxLayout* buttonsLayout = new QHBoxLayout(centralWidget);

	QPushButton* addPieceButton = new QPushButton("&Add", centralWidget);
	connect(addPieceButton, &QPushButton::released, this, &Dashboard::showAddPieceModal);
	buttonsLayout->addWidget(addPieceButton);

	removePieceButton = new QPushButton("&Remove", centralWidget);
	removePieceButton->setDisabled(true);
	connect(removePieceButton, &QPushButton::released, this, &Dashboard::deletePiece);
	buttonsLayout->addWidget(removePieceButton);

	mainLayout->addLayout(buttonsLayout);

	m_piecesList = new QListWidget(centralWidget);
	renderPiecesList(m_controller->GetAllPieces());
	mainLayout->addWidget(m_piecesList);
	connect(m_piecesList, &QListWidget::itemSelectionChanged, [&] { removePieceButton->setDisabled(false); });


	centralWidget->setLayout(mainLayout);
	centralWidget->setMinimumWidth(500);
	centralWidget->setMinimumHeight(500);
	this->setCentralWidget(centralWidget);
}

void Dashboard::showAddPieceModal()
{
	auto modal = new AddPieceModal(this);
	modal->show();
}

void Dashboard::renderPiecesList(std::vector<ChessPiece*> pieces)
{
	m_piecesList->clear();
	for (ChessPiece* piece : pieces)
	{
		auto listItem = new PieceListItem(piece->GetId(), QString::fromStdString(piece->ToString()), m_piecesList);
	}
}

void Dashboard::keyReleaseEvent(QKeyEvent* event)
{
	if (event->key() == Qt::Key_Z && (event->modifiers() & Qt::ControlModifier))
	{
		m_controller->Undo();
		filterPieces();
	}
	else if ((event->key() == Qt::Key_Y && (event->modifiers() & Qt::ControlModifier)))
	{
		m_controller->Redo();
		filterPieces();
	}

}

void Dashboard::deletePiece()
{
	for (auto selectedPiece : m_piecesList->selectedItems())
	{
		m_controller->RemovePieceById(((PieceListItem*)selectedPiece)->PieceId);
		m_piecesList->removeItemWidget(selectedPiece);
		delete selectedPiece;
	}
}

void Dashboard::filterPieces()
{
	int colorsDropdownValue = m_colorsDropdown->currentIndex();
	int typesDropdownValue = m_typesDropdown->currentIndex();

	if (colorsDropdownValue == 2 && typesDropdownValue == 3)
	{
		renderPiecesList(m_controller->GetAllPieces());
	}
	else if (colorsDropdownValue == 2)
	{
		renderPiecesList(m_controller->GetPiecesByType(Dashboard::indexToTypeName[typesDropdownValue]));
	}
	else if (typesDropdownValue == 3)
	{
		renderPiecesList(m_controller->GetPiecesByColor((ChessPiece::Color)colorsDropdownValue));
	}
	else
	{
		renderPiecesList(m_controller->GetPiecesByColorAndType(
			(ChessPiece::Color)colorsDropdownValue,
			Dashboard::indexToTypeName[typesDropdownValue])
		);
	}
}

Dashboard::Dashboard(PiecesController* controller, QWidget* parent) : QMainWindow(parent)
{
	m_controller = controller;
	setupDahsboard();
}

Dashboard::~Dashboard()
{
	delete m_controller;
}

void Dashboard::SavePiece(ChessPiece* piece)
{
	m_controller->AddPiece(piece);
	filterPieces();
}
