#pragma once

#include <qlistwidget.h>

class PieceListItem : public QListWidgetItem
{
public:
	PieceListItem(int pieceId, QString& label, QListWidget* parent = nullptr);

	int PieceId;
};

