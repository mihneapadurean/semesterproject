#include "AddPieceModal.h"

#include <qwidget.h>
#include <QHBoxLayout>

void AddPieceModal::savePiece()
{
	int pieceType = m_typeDropdown->currentIndex();
	ChessPiece::Color color = (ChessPiece::Color)m_colorDropdown->currentIndex();

	bool noErrors = true;
	if (m_xPosition->text().isEmpty() || m_yPosition->text().isEmpty())
	{
		return;
	}


	int xPos = m_xPosition->text().toInt();
	int yPos = m_yPosition->text().toInt();

	try
	{
		if (pieceType == 0)
		{
			if (m_pawnHeight->text().isEmpty())
			{
				return;
			}
			int height = m_pawnHeight->text().toInt();
			m_parentWindow->SavePiece(new Pawn(Point(xPos, yPos), color, height));
		}
		else if (pieceType == 1)
		{
			Knight::HorseBreeds breed = (Knight::HorseBreeds)m_knightBreedDropdown->currentIndex();
			m_parentWindow->SavePiece(new Knight(Point(xPos, yPos), color, breed));
		}
		else if (pieceType == 2)
		{
			std::string kingdomName = m_kingdomName->text().toStdString();
			m_parentWindow->SavePiece(new King(Point(xPos, yPos), color, kingdomName));
		}
		this->close();
		delete this;
	}
	catch (std::exception e)
	{
		return;
	}
}

AddPieceModal::AddPieceModal(Dashboard* parentWindow) : QMainWindow(parentWindow)
{
	m_parentWindow = parentWindow;
	setupModal();
}

AddPieceModal::~AddPieceModal()
{
}

void AddPieceModal::setupModal()
{
	QWidget* centralWidget = new QWidget(this);
	m_formLayout = new QFormLayout(this);

	QLabel* typeLabel = new QLabel("Type", this);
	m_typeDropdown = new QComboBox(this);
	m_typeDropdown->insertItem(0, QString::fromStdString(Pawn::Type()));
	m_typeDropdown->insertItem(1, QString::fromStdString(Knight::Type()));
	m_typeDropdown->insertItem(2, QString::fromStdString(King::Type()));

	connect(m_typeDropdown, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &AddPieceModal::updateFormForType);

	QLabel* colorLabel = new QLabel("Color", this);
	m_colorDropdown = new QComboBox(this);
	m_colorDropdown->insertItem(ChessPiece::White, QString::fromStdString(ChessPiece::ColorToString(ChessPiece::White)));
	m_colorDropdown->insertItem(ChessPiece::Black, QString::fromStdString(ChessPiece::ColorToString(ChessPiece::Black)));
	m_colorDropdown->setCurrentIndex(ChessPiece::White);

	QLabel* positionXLabel = new QLabel("X Coordinate", this);
	m_xPosition = new QLineEdit(this);
	m_xPosition->setValidator(new QIntValidator(0, 7, this));

	QLabel* positionYLabel = new QLabel("Y Coordinate", this);
	m_yPosition = new QLineEdit(this);
	m_yPosition->setValidator(new QIntValidator(0, 7, this));

	m_kingdomNameLabel = new QLabel("Kingdom name", this);
	m_kingdomName = new QLineEdit(this);

	m_knightBreedLabel = new QLabel("Knight breed", this);
	m_knightBreedDropdown = new QComboBox(this);
	m_knightBreedDropdown->insertItem(Knight::Arabian, QString::fromStdString(Knight::BreedToString(Knight::Arabian)));
	m_knightBreedDropdown->insertItem(Knight::Appaloosa, QString::fromStdString(Knight::BreedToString(Knight::Appaloosa)));
	m_knightBreedDropdown->insertItem(Knight::Morgan, QString::fromStdString(Knight::BreedToString(Knight::Morgan)));
	m_knightBreedDropdown->setCurrentIndex(Knight::Arabian);

	m_pawnHeightLabel = new QLabel("Height", this);
	m_pawnHeight = new QLineEdit(this);
	m_pawnHeight->setValidator(new QIntValidator(0, INT_MAX, this));

	m_formLayout->addRow(typeLabel, m_typeDropdown);
	m_formLayout->addRow(colorLabel, m_colorDropdown);

	QHBoxLayout* positionLayout = new QHBoxLayout(this);
	positionLayout->addWidget(positionXLabel);
	positionLayout->addWidget(m_xPosition);
	positionLayout->addWidget(positionYLabel);
	positionLayout->addWidget(m_yPosition);
	m_formLayout->addRow(positionLayout);

	m_formLayout->addRow(m_pawnHeightLabel, m_pawnHeight);
	m_formLayout->addRow(m_knightBreedLabel, m_knightBreedDropdown);
	m_formLayout->addRow(m_kingdomNameLabel, m_kingdomName);
	updateFormForType(m_typeDropdown->currentIndex());

	QPushButton* cancelButton = new QPushButton("&Cancel", this);
	QPushButton* saveButton = new QPushButton("&Save", this);
	QHBoxLayout* buttonsLayout = new QHBoxLayout(this);
	buttonsLayout->addStretch(0.6 * this->width());
	buttonsLayout->addWidget(cancelButton);
	buttonsLayout->addWidget(saveButton);
	m_formLayout->addRow(buttonsLayout);

	connect(cancelButton, &QPushButton::released, [=] { 
		this->close();
		delete this;
	});
	connect(saveButton, &QPushButton::released, this, &AddPieceModal::savePiece);

	centralWidget->setLayout(m_formLayout);
	this->setCentralWidget(centralWidget);
}

void AddPieceModal::updateFormForType(int typeIndex)
{
	if (typeIndex == 0)
	{
		m_pawnHeight->setDisabled(false);
		m_knightBreedDropdown->setDisabled(true);
		m_kingdomName->setDisabled(true);
		m_kingdomName->clear();
	}
	else if (typeIndex == 1)
	{
		m_pawnHeight->setDisabled(true);
		m_pawnHeight->clear();
		m_knightBreedDropdown->setDisabled(false);
		m_kingdomName->setDisabled(true);
		m_kingdomName->clear();
	}
	else if (typeIndex == 2)
	{
		m_pawnHeight->setDisabled(true);
		m_pawnHeight->clear();
		m_knightBreedDropdown->setDisabled(true);
		m_kingdomName->setDisabled(false);
	}
}