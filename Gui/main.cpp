#include <QtWidgets/QApplication>

#include "src/Dashboard.h"
#include "controller/PiecesController.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Dashboard dashboard(new PiecesController(new PiecesRepository("chesspieces.csv")));
    dashboard.show();

    return a.exec();
}
