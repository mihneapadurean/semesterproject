#pragma once
#include "Point.h"

struct Shape
{
	virtual bool Contains(const Point& p) const = 0;
};

