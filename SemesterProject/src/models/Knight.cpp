#include "Knight.h"

std::string Knight::BreedToString(Knight::HorseBreeds breed)
{
	switch (breed)
	{
	case Knight::Arabian:
		return "Arabian";
	case Knight::Appaloosa:
		return "Appaloosa";
	case Knight::Morgan:
		return "Morgan";
	default:
		return "Not a breed";
	}
}

Knight::Knight(int id, const Point& position, ChessPiece::Color color, Knight::HorseBreeds breed) : ChessPiece(id, position, color)
{
	m_breed = breed;
}

Knight::Knight(const Point& position, ChessPiece::Color color, Knight::HorseBreeds breed) : Knight(0, position, color, breed)
{
}

std::string Knight::GetType() const
{
	return "Knight";
}

std::string Knight::ToString() const
{
	return std::to_string(m_id) + ". " + ChessPiece::ColorToString(m_color) + " Knight (" + Knight::BreedToString(m_breed) + ") at: " + m_position.ToString();
}

Knight::HorseBreeds Knight::GetBreed() const
{
	return m_breed;
}

void Knight::SetBreed(Knight::HorseBreeds breed)
{
	m_breed = breed;
}

std::string Knight::Type()
{
	return "Knight";
}
