#pragma once
#include <string>
#include <vector>
#include "Point.h"
#include "Shape.h"

/**
*	Abstract class that represents the base for a chess piece
*/
class ChessPiece
{
public:
	static enum Color
	{
		White = 0, Black
	};
	/**
	*	Static method that returns a formatted name for each color from the enum
	*	@param color - Color
	*	@returns string
	*/
	static std::string ColorToString(ChessPiece::Color color);

protected:
	int m_id;
	Point m_position;
	ChessPiece::Color m_color;

public:
	/**
	*	Param constructor with explicit Id
	*	@param id - int
	*	@param position - const Point&
	*	@param color - Color
	*/
	ChessPiece(int id, const Point& position, ChessPiece::Color color);
	/**
	*	Param constrcutor without id -> will default the id to 0
	*	@param position - const Point&
	*	@param color - Color
	*/
	ChessPiece(const Point& position, ChessPiece::Color color);

	/**
	*	@returns int
	*/
	int GetId() const;
	/**
	*	@param id - int
	*/
	void SetId(int id);

	/**
	*	@returns const Point&
	*/
	const Point& GetPosition() const;
	/**
	*	@param const Point&
	*/
	void SetPosition(const Point& position);

	/**
	*	@returns Color
	*/
	ChessPiece::Color GetColor() const;
	/**
	*	@param color - Color
	*/
	void SetColor(ChessPiece::Color color);

	/**
	*	Pure virtual function that will return the type of each subclass
	*/
	virtual std::string GetType() const = 0;
	/**
	*	Pure virtual function that will return a string representation of each subclass
	*/
	virtual std::string ToString() const = 0;
};

