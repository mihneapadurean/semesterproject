#pragma once
#include "ChessPiece.h"

/**
*   Subclass of ChessPiece used for representing the knight piece. It also contains a the horse's breed + the available choices int he static enum HorseBreeds.
*/
class Knight :
    public ChessPiece
{
public:
    static enum HorseBreeds { Arabian = 0, Appaloosa, Morgan };
    static std::string BreedToString(Knight::HorseBreeds breed);

private:
    Knight::HorseBreeds m_breed;

public:
    /**
    *   Param constructor with explicit id
    *   @see ChessPiece(int id, const Point& position, Color color) for param explanaations
    *   @param breed - HorseBreeds
    */
    Knight(int id, const Point& position, ChessPiece::Color color, Knight::HorseBreeds breed);
    /**
    *   Param constructor with implicit id -> will default to 0
    *   @see ChessPiece(const Point& position, Color color) for param explanaations
    *   @param breed - HorseBreeds
    */
    Knight(const Point& position, ChessPiece::Color color, Knight::HorseBreeds breed);

    /**
    *   override of GetType pure virtual method of ChessPiece
    *   @see ChessPiece::GetType()
    */
    std::string GetType() const override;
    /**
    *   override of ToString pure virtual method of ChessPiece
    *   @see ChessPiece::ToString()
    */
    std::string ToString() const override;

    /**
    *   @returns HorseBreeds
    */
    Knight::HorseBreeds GetBreed() const;
    /**
    *   @param breed - HorseBreeds
    */
    void SetBreed(Knight::HorseBreeds breed);

    /**
    *   Static method that returns the type of the knight subclass
    *   @returns string
    */
    static std::string Type();
};

