#pragma once
#include <string>
/**
*	Used for storing 2D points
*/
struct Point
{
	int X, Y;

	/**
	* Default constructor, initializes x, y with 0
	*/
	Point();
	/**
	* Parametrized constructor
	* @param x - integer
	* @param y - integer
	*/
	Point(int x, int y);

	inline std::string ToString() const { return "(" + std::to_string(X) + ", " + std::to_string(Y) + ")"; }

	/**
	* Equality overload
	* @return bool - true if the points have the same coordinates
	*/
	friend bool operator==(const Point& lhs, const Point& rhs);
	friend bool operator!=(const Point& lhs, const Point& rhs);
};

