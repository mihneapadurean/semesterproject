#include "King.h"

King::King(int id, const Point& position, ChessPiece::Color color, std::string countryName) : ChessPiece(id, position, color)
{
	m_kingdomName = countryName;
}

King::King(const Point& position, ChessPiece::Color color, std::string kingdomName) : King(0, position, color, kingdomName)
{
}

std::string King::GetKingdomName() const
{
	return m_kingdomName;
}

void King::SetKingdomName(std::string kingdomName)
{
	m_kingdomName = kingdomName;
}

std::string King::Type()
{
	return "King";
}

std::string King::GetType() const
{
	return "King";
}

std::string King::ToString() const
{
	return std::to_string(m_id)+ ". " +  ChessPiece::ColorToString(m_color) + " King of " + m_kingdomName + " at: " + m_position.ToString();
}
