#pragma once
#include "Shape.h"
/**
*	Used for storing a 2D rectangle. It is represented by its bottom left corner (as a point) and its length and height
*/
struct Rectangle : public Shape
{
	Point BottomLeft;
	int Length, Height;

	/**
	*	Param constructor
	*	@param xBottomLeft - integer
	*	@param yBottomLeft - integer
	*	@param length - integer
	*	@param height - integer 
	*/
	Rectangle(int xBottomLeft, int yBottomLeft, int length, int height);
	/**
	*	Param constructor
	*	@param BottomLeft - const Point&
	*	@param length - integer
	*	@param height - integer
	*/
	Rectangle(const Point& bottomLeft, int length, int height);

	/**
	*	Returns if a point is contained inside the rectangle (overload of pure virtual method inside Shape)
	*	@param p - const Point&
	*	@returns bool - true if the point is inside the rectangle
	*/
	bool Contains(const Point& p) const override;

	~Rectangle();
};

