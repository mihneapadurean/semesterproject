#pragma once
#include "ChessPiece.h"

/**
*	Struct that represents an operation on a chessPiece (add or remove)
*/
struct Operation
{
	static enum OperationTypes { Add = 0, Remove };

	OperationTypes OperationType;
	ChessPiece* TargetPiece;
};

