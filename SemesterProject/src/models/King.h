#pragma once
#include "ChessPiece.h"
/**
*   Subclass of ChessPiece used for representing the king piece. It also contains a kingdomName if needed.
*/
class King :
    public ChessPiece
{
private:
    std::string m_kingdomName;
public:
    /**
    *   Param constructor with explicit id
    *   @see ChessPiece(int id, const Point& position, Color color) for param explanaations
    *   @param kingdomName - string
    */
    King(int id, const Point& position, ChessPiece::Color color, std::string kingdomName);
    /**
    *   Param constructor with implicit id -> will default to 0
    *   @see ChessPiece(const Point& position, Color color) for param explanaations
    *   @param kingdomName - string
    */
    King(const Point& position, ChessPiece::Color color, std::string kingdomName);

    /**
    *   override of GetType pure virtual method of ChessPiece
    *   @see ChessPiece::GetType()
    */
    std::string GetType() const override;
    /**
    *   override of ToString pure virtual method of ChessPiece
    *   @see ChessPiece::ToString()
    */
    std::string ToString() const override;

    /**
    *   @returns string
    */
    std::string GetKingdomName() const;
    /**
    *   @param kingdomName - string
    */
    void SetKingdomName(std::string kingdomName);
    
    /**
    *   Static method that returns the type of the king subclass  
    *   @returns string
    */
    static std::string Type();
};

