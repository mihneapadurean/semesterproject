#include "Pawn.h"

Pawn::Pawn(int id, const Point& position, ChessPiece::Color color, int height) : ChessPiece(id, position, color)
{
	m_height = height;
}

Pawn::Pawn(const Point& position, ChessPiece::Color color, int height) : Pawn(0, position, color, height)
{
}

std::string Pawn::GetType() const
{
	return "Pawn";
}

std::string Pawn::ToString() const
{
	return std::to_string(m_id) + ". " + ChessPiece::ColorToString(m_color) + " Pawn (" + std::to_string(m_height) + " cm) at: " + m_position.ToString();
}

int Pawn::GetHeight() const
{
	return m_height;
}

void Pawn::SetHeight(int height)
{
	m_height = height;
}

std::string Pawn::Type()
{
	return "Pawn";
}
