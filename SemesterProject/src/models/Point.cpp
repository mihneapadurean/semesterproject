#include "Point.h"

Point::Point() : Point(0, 0)
{
}

Point::Point(int x, int y) : X(x), Y(y)
{
}

bool operator==(const Point& lhs, const Point& rhs)
{
    return lhs.X == rhs.X && lhs.Y == rhs.Y;
}

bool operator!=(const Point& lhs, const Point& rhs)
{
    return !(lhs == rhs);
}
