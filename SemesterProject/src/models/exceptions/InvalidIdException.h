#pragma once
#include <exception>

class InvalidIdException : public std::exception
{
public:
	const char* what() const throw()
	{
		return "The id can't be found";
	}
};