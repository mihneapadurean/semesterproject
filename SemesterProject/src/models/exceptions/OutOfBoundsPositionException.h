#pragma once
#include <exception>

class OutOfBoundsPositionException : std::exception
{
public:
	const char* what() const throw()
	{
		return "The position is out of the table's bounds";
	}
};