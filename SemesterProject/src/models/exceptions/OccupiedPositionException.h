#pragma once
#include <exception>

class OccupiedPositionException : std::exception
{
public:
	const char* what() const throw()
	{
		return "The position is already taken";
	}
};