#pragma once
#include <exception>

class CorruptedDatabaseException : std::exception
{
public:
	const char* what() const throw()
	{
		return "The database file is corrupted";
	}
};