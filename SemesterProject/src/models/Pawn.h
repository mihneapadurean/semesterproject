#pragma once
#include "ChessPiece.h"

/**
*   Subclass of ChessPiece used for representing the pawn piece. It also contains the height.
*/
class Pawn :
    public ChessPiece
{
private:
    int m_height;

public:
    /**
    *   Param constructor with explicit id
    *   @see ChessPiece(int id, const Point& position, Color color) for param explanaations
    *   @param height - int
    */
    Pawn(int id, const Point& position, ChessPiece::Color color, int height);
    /**
    *   Param constructor with implicit id -> will default to 0
    *   @see ChessPiece(const Point& position, Color color) for param explanaations
    *   @param height - int
    */
    Pawn(const Point& position, ChessPiece::Color color, int height);

    /**
    *   override of GetType pure virtual method of ChessPiece
    *   @see ChessPiece::GetType()
    */
    std::string GetType() const override;
    /**
    *   override of ToString pure virtual method of ChessPiece
    *   @see ChessPiece::ToString()
    */
    std::string ToString() const override;

    /**
    *   @returns int
    */
    int GetHeight() const;
    /**
    *   @param height - int
    */
    void SetHeight(int height);

    /**
    *   Static method that returns the type of the pawn subclass
    *   @returns string
    */
    static std::string Type();
};