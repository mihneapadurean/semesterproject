#include "ChessPiece.h"

std::string ChessPiece::ColorToString(Color color)
{
	switch (color)
	{
	case ChessPiece::White:
		return "White";
	case ChessPiece::Black:
		return "Black";
	default:
		return "Not a color";
	}
}

ChessPiece::ChessPiece(int id, const Point& position, ChessPiece::Color color) : m_id(id), m_color(color)
{
	m_position = Point(position.X, position.Y);
}

ChessPiece::ChessPiece(const Point& position, ChessPiece::Color color) : ChessPiece(0, position, color)
{
}

int ChessPiece::GetId() const
{
	return m_id;
}

void ChessPiece::SetId(int id)
{
	m_id = id;
}

const Point& ChessPiece::GetPosition() const
{
	return m_position;
}

void ChessPiece::SetPosition(const Point& position)
{
	m_position = Point(position.X, position.Y);
}

ChessPiece::Color ChessPiece::GetColor() const
{
	return m_color;
}

void ChessPiece::SetColor(ChessPiece::Color color)
{
	m_color = color;
}