#include "Rectangle.h"

Rectangle::Rectangle(int xBottomLeft, int yBottomLeft, int length, int height)
{
	BottomLeft = Point(xBottomLeft, yBottomLeft);
	Length = length;
	Height = height;
}

Rectangle::Rectangle(const Point& bottomLeft, int length, int height) : Rectangle(bottomLeft.X, bottomLeft.Y, length, height)
{
}

bool Rectangle::Contains(const Point& p) const
{
	return p.X >= BottomLeft.X && p.X <= BottomLeft.X + Length - 1 && p.Y >= BottomLeft.Y && p.Y <= BottomLeft.Y + Height - 1;
}

Rectangle::~Rectangle()
{
}
