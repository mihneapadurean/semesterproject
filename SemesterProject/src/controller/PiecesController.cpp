#include "PiecesController.h"

PiecesController::PiecesController(PiecesRepository* repository) : m_repo(repository)
{
	m_repo->Load();
}

std::vector<ChessPiece*> PiecesController::GetAllPieces() const
{
	return m_repo->GetAll();
}

std::vector<ChessPiece*> PiecesController::GetPiecesByType(std::string type) const
{
	static std::string pieceType;
	pieceType = type;
	return m_repo->GetByFilter([](ChessPiece* piece) { return piece->GetType() == pieceType; });
}

std::vector<ChessPiece*> PiecesController::GetPiecesByColor(ChessPiece::Color color) const
{
	static ChessPiece::Color colorStatic;
	colorStatic = color;
	return m_repo->GetByFilter([](ChessPiece* p) { return p->GetColor() == colorStatic; });
}

std::vector<ChessPiece*> PiecesController::GetPiecesByColorAndType(ChessPiece::Color color, std::string type) const
{
	static ChessPiece::Color colorStatic;
	static std::string typeStatic;
	colorStatic = color;
	typeStatic = type;
	return m_repo->GetByFilter([](ChessPiece* piece) { 
		return piece->GetType() == typeStatic && piece->GetColor() == colorStatic;
	});
}

void PiecesController::AddPiece(ChessPiece* piece)
{
	m_repo->Add(piece);
}

void PiecesController::RemovePieceById(int id)
{
	m_repo->RemoveById(id);
}

void PiecesController::Undo()
{
	m_repo->Undo();
}

void PiecesController::Redo()
{
	m_repo->Redo();
}

PiecesController::~PiecesController()
{
	m_repo->Save();
	delete m_repo;
}
