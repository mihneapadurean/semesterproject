#pragma once
#include "../repository/PiecesRepository.h"

/**
*	Controller for communicating with a pieces repository
*/
class PiecesController
{
private:
	PiecesRepository* m_repo;

public:
	/**
	*	Constructor for the controller with an injected repository. Will also call load on the repository
	*	@param repository - PiecesRepository*
	*/
	PiecesController(PiecesRepository* repository);

	/**
	*	Returns all the pieces in the repository
	*	@returns vector<ChessPiece*>
	*/
	std::vector<ChessPiece*> GetAllPieces() const;
	/**
	*	Returns all the pieces in the repository with a given type
	*	@param type - string
	*	@returns vector<ChessPiece*>
	*/
	std::vector<ChessPiece*> GetPiecesByType(std::string type) const;
	/**
	*	Returns all the pieces in the repository with a given color
	*	@param color - Color
	*	@returns vector<ChessPiece*>
	*/
	std::vector<ChessPiece*> GetPiecesByColor(ChessPiece::Color color) const;

	/**
	*	Returns all the pieces in the repository with a given color and type
	*	@param color - Color
	*	@param type - string
	*	@returns vector<ChessPiece*>
	*/
	std::vector<ChessPiece*> GetPiecesByColorAndType(ChessPiece::Color color, std::string type) const;

	/**
	*	@param piece - ChessPiece*
	*	@throws OutOfBoundsPositionException - the position of the piece is not inside the table
	*	@throws OccupiedPositionException - the position of the piece is already taken by another piece
	*	@throws InvalidIdException - the id is already taken
	*/
	void AddPiece(ChessPiece* piece);

	/**
	*	@param id - int
	*	@throws InvalidIdException - the id is not found
	*/
	void RemovePieceById(int id);


	void Undo();
	void Redo();

	~PiecesController();
};

