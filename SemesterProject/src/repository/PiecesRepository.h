#pragma once
#include <vector>
#include <stack>
#include "../models/ChessPiece.h"
#include "../models/Shape.h"	
#include "../models/Rectangle.h"
#include "../models/exceptions/InvalidIdException.h"
#include "../models/exceptions/OccupiedPositionException.h"
#include "../models/exceptions/OutOfBoundsPositionException.h"
#include "../models/Operation.h"
#include "ChessPieceCsvHelpers.h"

/**
*	Repository that holds a collection of ChessPieces + Read/Add/Delete operations on them. Also a, undo/redo mechanism is implemented.
*	The repository can be prefilled with values from a csv file and the contens can be saved to a specified csv file.
*/
class PiecesRepository
{
private:
	std::vector<ChessPiece*> m_pieces;
	Shape* m_table;
	std::string m_dataStoragePath;

	std::stack<Operation> m_completedOperations;
	std::stack<Operation> m_undoneOperations;

	ChessPiece* m_RemoveById(int id);

	void m_FreePiecesAndUndo();
	
	int m_lastId = 0;

public:
	/**
	*	Constructor which sets the file for persistent storage (as a csv)
	*	@param dataStoragePath - string
	*/
	PiecesRepository(std::string dataStoragePath);

	/**
	*	Returns all chessPieces as pointers.
	*	@returns vector<ChessPiece*>
	*/
	std::vector<ChessPiece*> GetAll() const;
	/**
	*	Returns all pieces that satisfy a predicate chessPieces as pointers.
	*	@param filter - func<ChessPiece*, bool>
	*	@returns vector<ChessPiece*>
	*/
	std::vector<ChessPiece*> GetByFilter(bool (*filter)(ChessPiece*)) const;

	/**
	*	Adds a new ChessPiece with the next available id
	*	@param newPiece - ChessPiece*
	*	@throws OutOfBoundsPositionException - the position of the piece is not inside the table
	*	@throws OccupiedPositionException - the position of the piece is already taken by another piece
	*/
	void Add(ChessPiece* newPiece);
	/**
	*	Adds a new ChessPiece with the given id
	*	@param newPiece - ChessPiece*
	*	@throws OutOfBoundsPositionException - the position of the piece is not inside the table
	*	@throws OccupiedPositionException - the position of the piece is already taken by another piece
	*	@throws InvalidIdException - the id is already taken
	*/
	void AddWithId(ChessPiece* newPiece);

	/**
	*	Removes a piece from the collection based on its id
	*	@param id - int
	*	@throws InvalidIdException - the id is not found
	*/
	void RemoveById(int id);

	/**
	*	Undo on the previous Add/Remove operation. If there is no such operation to be performed, nothing happens.
	*/
	void Undo();
	/**
	*	Redo on a previous undone Add/Remove operation. If there is no such operation to be performed, nothing happens.
	*/
	void Redo();

	/**
	*	Saves the current pieces inside the repository as a csv to the filePath supplied in the constructor (dataStoragePath)
	*/
	void Save() const;
	/**
	*	Loads all the pieces from the file supplied in the constructor (dataStoragePath) to the repository
	*	@throws CorruptedDatabaseException - the csv in the filepath is not valid
	*									   - the csv is valid but the pieces information is invalid (2 with the same id)
	*/
	void Load();

	/*
	*	Frees the memory occupied by the chessPieces.
	*/
	~PiecesRepository();
};

