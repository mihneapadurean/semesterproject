#include "ChessPieceCsvHelpers.h"

void ChessPieceCsvHelpers::passDelimiter(std::ifstream& in)
{
	char delim;
	in >> delim;
	if (delim != ChessPieceCsvHelpers::csv_delimiter)
		throw CorruptedDatabaseException();
}

int ChessPieceCsvHelpers::readInteger(std::ifstream& in)
{
	int a;
	if (in >> a)
	{
		return a;
	}
	else
	{
		throw CorruptedDatabaseException();
	}
}

ChessPiece::Color ChessPieceCsvHelpers::readColor(std::ifstream& in)
{
	int c;
	if (in >> c && (c == ChessPiece::White || c == ChessPiece::Black))
	{
		return (ChessPiece::Color)c;
	}
	else
	{
		throw CorruptedDatabaseException();
	}
}

Knight::HorseBreeds ChessPieceCsvHelpers::readBreed(std::ifstream& in)
{
	int b;
	if (in >> b && (b == Knight::Arabian || b == Knight::Appaloosa || b == Knight::Morgan))
	{
		return (Knight::HorseBreeds)b;
	}
	else
	{
		throw CorruptedDatabaseException();
	}
}

void ChessPieceCsvHelpers::WriteAllToFile(std::vector<ChessPiece*> pieces, std::string filePath)
{
	std::string output;
	for (ChessPiece* piece : pieces)
	{
		Point position = piece->GetPosition();
		std::string type = piece->GetType();
		output +=
			std::to_string(piece->GetId()) + "," +
			type + "," +
			std::to_string(position.X) + "," +
			std::to_string(position.Y) + "," +
			std::to_string(piece->GetColor()) + "," +
			(type == King::Type() ? ((King*)piece)->GetKingdomName() : "") + "," +
			(type == Knight::Type() ? std::to_string(((Knight*)piece)->GetBreed()) : "") + "," +
			(type == Pawn::Type() ? std::to_string(((Pawn*)piece)->GetHeight()) : "") + ",";
	}

	std::ofstream out(filePath, std::ofstream::trunc);
	out << output;
	out.close();
}

std::vector<ChessPiece*> ChessPieceCsvHelpers::ReadAllFromFile(std::string filePath)
{
	std::vector<ChessPiece*> result;

	std::ifstream in(filePath);

	int id;
	std::string type;
	int pos_x, pos_y;
	ChessPiece::Color color;
	std::string kingdomName;
	Knight::HorseBreeds breed;
	int height;
	while (in >> id)
	{
		auto passDelim = ChessPieceCsvHelpers::passDelimiter;
		passDelim(in);

		std::getline(in, type, ',');

		pos_x = ChessPieceCsvHelpers::readInteger(in);
		passDelim(in);
		pos_y = ChessPieceCsvHelpers::readInteger(in);
		passDelim(in);

		color = ChessPieceCsvHelpers::readColor(in);
		passDelim(in);

		std::getline(in, kingdomName, ',');

		if (type == Knight::Type())
		{
			breed = ChessPieceCsvHelpers::readBreed(in);
		}
		passDelim(in);

		if (type == Pawn::Type())
		{
			height = ChessPieceCsvHelpers::readInteger(in);
		}
		passDelim(in);

		if (type == King::Type())
		{
			result.push_back(new King(id, Point(pos_x, pos_y), color, kingdomName));
		}
		else if (type == Knight::Type())
		{
			result.push_back(new Knight(id, Point(pos_x, pos_y), color, breed));
		}
		else if (type == Pawn::Type())
		{
			result.push_back(new Pawn(id, Point(pos_x, pos_y), color, height));
		}
		else
		{
			throw CorruptedDatabaseException();
		}
	}

	in.close();
	return result;
}
