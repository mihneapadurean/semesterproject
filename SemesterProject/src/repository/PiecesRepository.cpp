#include "PiecesRepository.h"

ChessPiece* PiecesRepository::m_RemoveById(int id)
{
    for (auto it = m_pieces.begin(); it != m_pieces.end(); ++it)
    {
        if ((*it)->GetId() == id)
        {
            ChessPiece* removedPiece = *it;
            m_pieces.erase(it);
            return removedPiece;
        }
    }
    return nullptr;
}

void PiecesRepository::m_FreePiecesAndUndo()
{
    for (ChessPiece* piece : m_pieces)
    {
        delete piece;
    }
    m_pieces.clear();

    while (!m_completedOperations.empty())
    {
        if (m_completedOperations.top().OperationType == Operation::Remove)
        {
            delete m_completedOperations.top().TargetPiece;
        }
        m_completedOperations.pop();
    }

    while (!m_undoneOperations.empty())
    {
        if (m_undoneOperations.top().OperationType == Operation::Add)
        {
            delete m_undoneOperations.top().TargetPiece;
        }
        m_undoneOperations.pop();
    }
    m_lastId = 0;
}

PiecesRepository::PiecesRepository(std::string dataStoragePath)
{
    m_table = new Rectangle(0, 0, 8, 8);
    m_dataStoragePath = dataStoragePath;
}

std::vector<ChessPiece*> PiecesRepository::GetAll() const
{
    return m_pieces;
}

std::vector<ChessPiece*> PiecesRepository::GetByFilter(bool(*filter)(ChessPiece*)) const
{
    std::vector<ChessPiece*> result;
    for (ChessPiece* piece : m_pieces)
    {
        if (filter(piece))
        {
            result.push_back(piece);
        }
    }
    return result;
}

void PiecesRepository::Add(ChessPiece* newPiece)
{
    newPiece->SetId(m_lastId + 1);
    AddWithId(newPiece);
}

void PiecesRepository::AddWithId(ChessPiece* newPiece)
{
    if (!m_table->Contains(newPiece->GetPosition()))
    {
        throw OutOfBoundsPositionException();
    }
    for (ChessPiece* piece : m_pieces)
    {
        if (piece->GetPosition() == newPiece->GetPosition())
        {
            throw OccupiedPositionException();
        }
        if (piece->GetId() == newPiece->GetId())
        {
            throw InvalidIdException();
        }
    }

    m_pieces.push_back(newPiece);
    if (m_lastId < newPiece->GetId())
    {
        m_lastId = newPiece->GetId();
    }
    m_completedOperations.push(Operation{ Operation::Add, newPiece });
}

void PiecesRepository::RemoveById(int id)
{
    ChessPiece* removedPiece = m_RemoveById(id);
    if (removedPiece == nullptr)
    {
        throw InvalidIdException();
    }
    m_completedOperations.push(Operation{ Operation::Remove, removedPiece });
}

void PiecesRepository::Undo()
{
    if (m_completedOperations.empty())
        return;
    
    Operation lastOperation = m_completedOperations.top();
    m_completedOperations.pop();

    switch (lastOperation.OperationType)
    {
    case Operation::Add:
        m_RemoveById(lastOperation.TargetPiece->GetId());
        m_undoneOperations.push(lastOperation);
        return;
    case Operation::Remove:
        m_pieces.push_back(lastOperation.TargetPiece);
        m_undoneOperations.push(lastOperation);
        return;
    }
}

void PiecesRepository::Redo()
{
    if (m_undoneOperations.empty())
        return;

    Operation lastUndoneOperation = m_undoneOperations.top();
    m_undoneOperations.pop();

    switch (lastUndoneOperation.OperationType)
    {
    case Operation::Add:
        m_pieces.push_back(lastUndoneOperation.TargetPiece);
        m_completedOperations.push(lastUndoneOperation);
        return;
    case Operation::Remove:
        m_RemoveById(lastUndoneOperation.TargetPiece->GetId());
        m_completedOperations.push(lastUndoneOperation);
        return;
    }
}

void PiecesRepository::Save() const
{
    ChessPieceCsvHelpers::WriteAllToFile(m_pieces, m_dataStoragePath);
}

void PiecesRepository::Load()
{
    std::vector<ChessPiece*> savedPieces =  ChessPieceCsvHelpers::ReadAllFromFile(m_dataStoragePath);

    auto corruptedCleanUp = [&]()
    {
        for (ChessPiece* piece : m_pieces)
        {
            delete piece;
        }
        m_FreePiecesAndUndo();
        throw CorruptedDatabaseException();
    };

    m_FreePiecesAndUndo();
    try
    {
        for (ChessPiece* piece : savedPieces)
        {
            AddWithId(piece);
        }
        m_completedOperations = std::stack<Operation>();
    }
    catch (OutOfBoundsPositionException e)
    {
        corruptedCleanUp();
    }
    catch (OccupiedPositionException e)
    {
        corruptedCleanUp();
    }
    catch (InvalidIdException e)
    {
        corruptedCleanUp();
    }
}

PiecesRepository::~PiecesRepository()
{
    delete m_table;
    m_FreePiecesAndUndo();
}
