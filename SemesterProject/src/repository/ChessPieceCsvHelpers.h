#pragma once
#include <fstream>
#include "../models/ChessPiece.h"
#include "../models/King.h"
#include "../models/Knight.h"
#include "../models/Pawn.h"
#include "../models/exceptions/CorruptedDatabaseException.h"

/**
*	Static class used for saving/loading chessPieces from a csv file
*/
class ChessPieceCsvHelpers
{
private:
	const static char csv_delimiter = ',';

	static void passDelimiter(std::ifstream& in);
	static int readInteger(std::ifstream& in);
	static ChessPiece::Color readColor(std::ifstream& in);
	static Knight::HorseBreeds readBreed(std::ifstream& in);

	ChessPieceCsvHelpers();

public:
	/**
	*	Saves all the pieces in the vector to a specified file as csv
	*	@param pieces: vector<ChessPieces*>
	*	@param filePath: string
 	*/
	static void WriteAllToFile(std::vector<ChessPiece*> pieces, std::string filePath);
	/**
	*	Loads all the pieces from a csv file in a vector
	*	@param filePath: string
	*	@returns vector<ChessPieces*>
	*	@throws CorruptedDatabaseException if the csv is invalid
	*/
	static std::vector<ChessPiece*> ReadAllFromFile(std::string filePath);
};