#include "Tests.h"

void Test_Point();
void Test_Rectangle();
void Test_King();
void Test_Knight();
void Test_Pawn();
void Test_ChessPieceCsvHelpers();
void Test_Repository();

void RunTests()
{
	Test_Point();
	Test_Rectangle();
	Test_King();
	Test_Knight();
	Test_Pawn();
	Test_ChessPieceCsvHelpers();
	Test_Repository();
}

//Point
void Test_Point()
{
	Point defaultPoint;
	assert(defaultPoint.X == 0 && defaultPoint.Y == 0);

	Point p(1, -1);
	assert(p.X == 1 && p.Y == -1);

	assert(p == Point(1, -1));
	assert(p != defaultPoint);

	std::cout << "Point tests complete" << std::endl;
}

//Rectangle
void Test_Rectangle()
{
	Rectangle r1(1, -1, 2, 5);
	assert(r1.BottomLeft.X == 1 && r1.BottomLeft.Y == -1 && r1.Length == 2 && r1.Height == 5);

	Rectangle r2(Point(0, 0), 5, 2);
	assert(r2.BottomLeft == Point(0, 0) && r2.Length == 5 && r2.Height == 2);

	Shape* rAsPointer = &r2;
	assert(rAsPointer->Contains(Point(1, 1)));
	assert(!rAsPointer->Contains(Point(10, 5)));

	std::cout << "Rectangle tests complete" << std::endl;
}

//ChessPiece Tests
// King + ChessPiece

void Test_King()
{
	assert(King::Type() == "King");

	King explicitKing(1, Point(0, 0), ChessPiece::White, "The Wild West");
	assert(explicitKing.GetKingdomName() == "The Wild West");

	ChessPiece* kingAsChessPiece = &explicitKing;
	assert(
		kingAsChessPiece->GetId() == 1 &&
		kingAsChessPiece->GetPosition() == Point(0, 0) &&
		kingAsChessPiece->GetColor() == ChessPiece::White &&
		kingAsChessPiece->GetType() == King::Type() &&
		kingAsChessPiece->ToString() == "1. White King of The Wild West at: (0, 0)"
	);

	explicitKing.SetKingdomName("The Tame East");
	assert(explicitKing.GetKingdomName() == "The Tame East");

	kingAsChessPiece->SetId(2);
	kingAsChessPiece->SetPosition(Point(1, 1));
	kingAsChessPiece->SetColor(ChessPiece::Black);
	assert(
		kingAsChessPiece->GetId() == 2 &&
		kingAsChessPiece->GetPosition() == Point(1, 1) &&
		kingAsChessPiece->GetColor() == ChessPiece::Black &&
		kingAsChessPiece->ToString() == "2. Black King of The Tame East at: (1, 1)"
	);

	std::cout << "King & ChessPiece polymoprhism tests complete" << std::endl;
}

//Knight
void Test_Knight()
{
	assert(Knight::Type() == "Knight");

	Knight defaultKnight(Point(0, 0), ChessPiece::White, Knight::Morgan);
	assert(defaultKnight.GetId() == 0);

	Knight explicitKnight(1, Point(0, 0), ChessPiece::White, Knight::Appaloosa);
	assert(
		explicitKnight.GetBreed() == Knight::Appaloosa &&
		explicitKnight.GetType() == Knight::Type() &&
		explicitKnight.ToString() == "1. White Knight (Appaloosa) at: (0, 0)"
	);

	explicitKnight.SetBreed(Knight::Arabian);
	assert(explicitKnight.GetBreed() == Knight::Arabian);

	std::cout << "Knight tests complete" << std::endl;
}

//Pawn
void Test_Pawn()
{
	assert(Pawn::Type() == "Pawn");

	Pawn defaultPawn(Point(0, 0), ChessPiece::White, 100);
	assert(defaultPawn.GetId() == 0);

	Pawn explicitPawn(1, Point(0, 0), ChessPiece::White, 100);
	assert(
		explicitPawn.GetHeight() == 100 &&
		explicitPawn.GetType() == Pawn::Type() &&
		explicitPawn.ToString() == "1. White Pawn (100 cm) at: (0, 0)"
	);

	explicitPawn.SetHeight(200);
	assert(explicitPawn.GetHeight() == 200);

	std::cout << "Pawn tests complete" << std::endl;
}

//Test Csv helper
void Test_ChessPieceCsvHelpers()
{
	std::string testFilePath = "test_csvHelpers.txt";
	assert(ChessPieceCsvHelpers::ReadAllFromFile(testFilePath) == std::vector<ChessPiece*>());

	King p1(1, Point(0, 0), ChessPiece::White, "Atlantida");
	Knight p2(2, Point(1, 1), ChessPiece::Black, Knight::Appaloosa);
	Pawn p3(3, Point(2, 2), ChessPiece::Black, 100);
	std::vector<ChessPiece*> pieces = std::vector<ChessPiece*>{ &p1, &p2, &p3 };

	ChessPieceCsvHelpers::WriteAllToFile(pieces, testFilePath);
	
	std::fstream in(testFilePath);
	std::string csv;
	std::getline(in, csv);
	assert(csv == "1,King,0,0,0,Atlantida,,,2,Knight,1,1,1,,1,,3,Pawn,2,2,1,,,100,");
	in.close();

	auto readPieces = ChessPieceCsvHelpers::ReadAllFromFile(testFilePath);
	assert(
		readPieces[0]->GetId() == p1.GetId() && readPieces[0]->GetType() == p1.GetType() &&
		readPieces[1]->GetId() == p2.GetId() && readPieces[1]->GetType() == p2.GetType() &&
		readPieces[2]->GetId() == p3.GetId() && readPieces[2]->GetType() == p3.GetType()
	);

	std::remove(&testFilePath[0]);
	std::cout << "Csv tests complete" << std::endl;
}

void Test_Repository()
{
	std::string testFilePath = "test_persistence.txt";
	PiecesRepository repo(testFilePath);

	//empty repository
	assert(repo.GetAll() == std::vector<ChessPiece*>());

	//adding an element without id
	King* p1 = new King(Point(0, 0), ChessPiece::Black, "Nothing Special");
	repo.Add(p1);

	//testing the default id given to the piece
	assert(p1->GetId() == 1);

	std::vector<ChessPiece*> expectedResult{ p1 };
	assert(std::is_permutation(expectedResult.begin(), expectedResult.end(), repo.GetAll().begin()));

	//adding a piece with invalid id
	Pawn* p2 = new Pawn(p1->GetId(), Point(1, 1), ChessPiece::White, 180);
	try
	{
		repo.AddWithId(p2);
		assert(false);
	}
	catch (InvalidIdException _)
	{
		assert(true);
	}

	//adding a piece with a taken position
	p2->SetPosition(p1->GetPosition());
	try
	{
		repo.Add(p2);
		assert(false);
	}
	catch (OccupiedPositionException _)
	{
		assert(true);
	}

	//adding a piece with a position outside the table
	p2->SetPosition(Point(-1, -1));
	try
	{
		repo.Add(p2);
		assert(false);
	}
	catch (OutOfBoundsPositionException _)
	{
		assert(true);
	}


	//adding the element with the next available id
	p2->SetPosition(Point(1, 1));
	repo.Add(p2);

	expectedResult = std::vector<ChessPiece*>{ p1, p2 };
	assert(std::is_permutation(expectedResult.begin(), expectedResult.end(), repo.GetAll().begin()));

	//get by filter
	//only pawns with height = p2->GetHeight()
	assert(repo.GetByFilter([](ChessPiece* c) { return c->GetType() == Pawn::Type() && ((Pawn*)c)->GetHeight() == 180; }) == std::vector<ChessPiece*>{ p2 });

	//only knight pieces
	assert(repo.GetByFilter([](ChessPiece* c) { return c->GetType() == Knight::Type(); }) == std::vector<ChessPiece*>());

	//delete by id
	repo.RemoveById(2);
	assert(repo.GetAll() == std::vector<ChessPiece*> { p1 });

	try
	{
		repo.RemoveById(2);
		assert(false);
	}
	catch (InvalidIdException _)
	{
		assert(true);
	}

	//undo
	repo.Undo();
	expectedResult = std::vector<ChessPiece*>{ p1, p2 };
	assert(std::is_permutation(expectedResult.begin(), expectedResult.end(), repo.GetAll().begin()));

	repo.Undo();
	expectedResult = std::vector<ChessPiece*>{ p1 };
	assert(std::is_permutation(expectedResult.begin(), expectedResult.end(), repo.GetAll().begin()));

	//redo
	repo.Redo();
	expectedResult = std::vector<ChessPiece*>{ p1, p2 };
	assert(std::is_permutation(expectedResult.begin(), expectedResult.end(), repo.GetAll().begin()));

	//load and save
	repo.Save();
	repo.RemoveById(p2->GetId());

	repo.Load();
	auto result = repo.GetAll();
	assert(
		result[0]->GetId() == 1 && result[0]->GetType() == "King" &&
		result[1]->GetId() == 2 && result[1]->GetType() == "Pawn"
	);

	std::remove(&testFilePath[0]);
	std::cout << "Repository tests complete" << std::endl;
}